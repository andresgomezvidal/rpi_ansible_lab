
- hosts: rpi4b8, centos9-node2            # centos9-node1 is the server
  become: yes
  gather_facts: yes
  remote_user: "{{ user_sudo }}"
  any_errors_fatal: true
  vars:
    user: "{{ user_gui }}"                # in this scenario user devops has uid 1001 in all servers
  pre_tasks:
    ## the default value is internal, which is what is needed for Debian, so we only need to set it with CentOS
    - name: set distribution facts
      set_fact:
        firewalld_zone: "public"
      when: ansible_facts.distribution == 'CentOS'

    - name: Create necessary directories
      file:
        path: "{{ item }}"
        state: directory
        owner: "{{ user }}"
        group: "{{ user_gui }}"
        mode: '0755'
      loop:
        - "{{ nfs_client_path }}/wars"
        - "{{ nfs_client_path }}/readonly"

    - name: Install with dependencies
      tags: install_nfs_client
      package:
        name: "{{ item }}"
        state: present
      with_items:
        - nfs-common
      when: ansible_facts.distribution == 'Debian'
    - name: Install with dependencies
      tags: install_nfs_client
      package:
        name: "{{ item }}"
        state: present
      with_items:
        - nfs-utils
      when: ansible_facts.distribution == 'CentOS'




    #- name: Block for selinux
      #tags: nfs_selinux
      #block:
      #- name: Set selinux flag and make it persistent
        #seboolean:
          #name: "{{ item }}"
          #state: yes
          #persistent: yes
        #loop:
          #- nfs_export_all_ro
          #- nfs_export_all_rw
      #- name: Allow {{ nfs_client_path }} in selinux
        #sefcontext:
          #target: "{{ nfs_client_path }}(/.*)?"
          #state: present
          #setype: public_content_rw_t
      #- name: Apply selinux context in {{ nfs_client_path }}
        #command: restorecon -irv {{ nfs_client_path }}



  post_tasks:
    - name: Waiting for port {{ nfs_port }} of the nfs server {{ nfs_server_ip }}
      tags: test_nfs_connectivity
      wait_for:
        host: "{{ nfs_server_ip }}"
        port: "{{ nfs_port }}"
        timeout: 60

    #- name: Show available nfs shares from {{ nfs_server_ip }} # fails when rpc-bind is not allowed through the firewall, it's only info, not needed for the mount
      #tags: test_nfs_connectivity
      #command: showmount -e {{ nfs_server_ip }}
      #register: showmount_available_command
      #changed_when: false
    #- debug:
        #var=showmount_available_command.stdout_lines

    ## the mount options are
    ## _netdev: mark it as network dependent, having to wait for the network to be ready to mount
    ## noatime: disable writing the file access time. Includes nodiratime
    ## nofail: continue if the nfs is not available
    ## nolock: disable file locking
    ## tcp or proto=tcp: indicates this should use the tcp protocol
    ## timeo: sets the timeout value
    ## retrans: amount of packets transmited every timeo seconds to check for timeout
    ## sec=sys: use local uid and gid
    ## local_lock=none: because nfs4 uses locks
    ## rw and ro are just suggestions from the client and don't matter, what matters is what the server has in their /etc/exports
    - name: Mount and save it in /etc/fstab
      tags: mount_nfs
      mount:
        path: "{{ nfs_client_path }}/wars"
        src: "{{ nfs_server_ip }}:{{ nfs_server_path }}/wars"
        fstype: nfs
        state: mounted
        opts: "defaults,_netdev,rw,noatime,nofail,vers=4.2,proto=tcp,timeo=60,retrans=2,sec=sys,local_lock=none"
      when: ansible_facts.distribution == 'Debian'
    - name: Mount and save it in /etc/fstab
      tags: mount_nfs
      mount:
        path: "{{ nfs_client_path }}/readonly"
        src: "{{ nfs_server_ip }}:{{ nfs_server_path }}/readonly"
        fstype: nfs
        state: mounted
        opts: "defaults,_netdev,ro,noatime,nofail,vers=4.2,proto=tcp,timeo=60,retrans=2,sec=sys,local_lock=none"
      when: ansible_facts.distribution == 'Debian'

    ## for some reason my CentOS client could not mount with fstab (no logs showing why) but manually worked fine
    ## after trying with multiple configurations I switched to systemd mounts
    - name: Block mounting with CentOS
      tags: mount_nfs
      block:
      - name: Create systemd mount entry
        copy:
          dest: /etc/systemd/system/opt-devops-nfs-{{ item }}.mount
          content: |
            [Install]
            RequiredBy=remote-fs.target     # this is needed to be able to enable the service

            [Unit]
            Documentation=man:fstab(5) man:systemd-fstab-generator(8)
            After=network.target

            [Mount]
            What=192.168.0.111:/opt/devops/nfs/{{ item }}
            Where=/opt/devops/nfs/{{ item }}
            Type=nfs4
            Options=_netdev,noatime,nofail,vers=4.2,proto=tcp,timeo=60,retrans=2,sec=sys,local_lock=none
          owner: root
          group: root
          mode: '0644'
        with_items:
          - wars
          - readonly
      - name: Daemon reload
        systemd:
          daemon_reload: yes
      - name: Start and enable the service
        systemd:
          name: "{{ item }}"
          state: started
          enabled: yes
        with_items:
          - opt-devops-nfs-wars.mount
          - opt-devops-nfs-readonly.mount
          - remote-fs.target                          # this calls the other services on boot
      when: ansible_facts.distribution == 'CentOS'


    - name: Test copy to {{ nfs_client_path }}/wars
      tags: test_nfs
      copy:
        dest: "{{ nfs_client_path }}/wars/prueba_{{ inventory_hostname }}"
        content: "prueba"
        owner: "{{ user }}"
        group: "{{ user_gui }}"
        mode: '0644'
    - name: Remove test in {{ nfs_client_path }}/wars
      tags: test_nfs
      file:
        state: absent
        path: "{{ nfs_client_path }}/wars/prueba_{{ inventory_hostname }}"
    - name: Test copy to {{ nfs_client_path }}/readonly (this should fail because it is readonly)
      tags: test_nfs
      copy:
        dest: "{{ nfs_client_path }}/readonly/prueba_{{ inventory_hostname }}"
        content: "prueba"
        owner: "{{ user }}"
        group: "{{ user_gui }}"
        mode: '0644'
      ignore_errors: true


  roles:
    - { role: allow_firewall_port, firewall_ports: [ { number: "{{ nfs_port }}", type: 'tcp'}, { number: "{{ nfs_port }}", type: 'udp'} ] }

    ## we could add these for more info from the clients, but there is no need in this case
    #- { role: allow_firewall_port, firewall_ports: [ { service: "mountd" } ] }
    #- { role: allow_firewall_port, firewall_ports: [ { service: "rpc-bind" } ] }
