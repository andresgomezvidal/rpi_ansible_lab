#!/usr/bin/env bash

if [[ "$1" = 'sudo' ]]; then
    MOD='sudo'
fi

echo '==========     user id     =========='
id
echo -e '\n\n'

echo '==========     hostname     =========='
hostname
echo -e '\n\n'

echo '==========     contents of /etc/hosts     =========='
$MOD cat /etc/hosts
echo -e '\n\n'

echo '==========     Files in /etc/ modified in the last 7 days     =========='
$MOD find /etc/ -type f -mtime -6 -exec ls -alh {} \; 2> /dev/null
echo -e '\n\n'

echo '==========     Dirs in /opt/ (maxdepth 2)     =========='
$MOD find /opt/ -maxdepth 2 -type d 2> /dev/null
echo -e '\n\n'

echo '==========     uname -a     =========='
uname -a
echo -e '\n'
echo '==========     cat /etc/*-release     =========='
$MOD cat /etc/*-release
echo -e '\n\n'


if [ -n "$(grep -v '^#' /etc/security/limits.conf)" ]; then
    echo "==========     grep -v '^#' /etc/security/limits.conf:     =========="
    grep -v '^#' /etc/security/limits.conf
    echo -e '\n\n'
fi

echo "==========     Analysis of limits and usage per user     =========="
if [[ "$1" = 'sudo' ]]; then
    limits_users="$(awk -F ':' '$0!~/nologin/ {print $1}' /etc/passwd |grep -vE 'bin|daemon|nobody|dbus|systemd-|uuidd|avahi|colord|polkitd|usbmux|lightdm|rtkit|ntp|geoclue|saned|nvidia|tss')"
else
    limits_users="$USER"
fi
while read -r loopuser; do
    procs="$(ps --no-headers -eo uname,pid,ppid,pcpu,pmem,args | awk ' $1=="'$loopuser'" ' |grep -v $$ | grep -v grep)"
    if [ -n "$procs" ]; then
        #if [ "$USER" != "$loopuser" ]; then
            #echo "Soft ulimits of user $loopuser:"
            #sudo su - $loopuser -c 'ulimit -Sa'
            #echo -e "\nHard ulimits of user $loopuser:"
            #sudo su - $loopuser -c 'ulimit -Ha'
            #echo -e '\n'
        #fi

        totalf=0
        totalsign=0
        totalprocs=$(wc -l <<< "$procs")
        if [ "$USER" = "$loopuser" ]; then
            limitsn="$(ulimit -Sn)"
            limithn="$(ulimit -Hn)"
            limitsu="$(ulimit -Su)"
            limithu="$(ulimit -Hu)"
            limitsi="$(ulimit -Si)"
            limithi="$(ulimit -Hi)"
        else
            limitsn="$(sudo su - $loopuser -c 'ulimit -Sn')"
            limithn="$(sudo su - $loopuser -c 'ulimit -Hn')"
            limitsu="$(sudo su - $loopuser -c 'ulimit -Su')"
            limithu="$(sudo su - $loopuser -c 'ulimit -Hu')"
            limitsi="$(sudo su - $loopuser -c 'ulimit -Si')"
            limithi="$(sudo su - $loopuser -c 'ulimit -Hi')"
        fi
        while read -r i; do
            pid="$(awk '{print $2}' <<< "$i")"
            pfd="$($MOD ls -1 /proc/$pid/fd 2> /dev/null | wc -l)"
            if [[ -n "$pfd" ]]; then
                totalf=$(( totalf + pfd ))
                if [[ "$pfd" -ge "$limitsn" ]]; then
                    echo "process of user $loopuser has $pfd open files, while the soft limit is $limitsn (the hard limit is $limithn):"
                    echo "$i"
                fi
            fi
            psign="$(awk '$1=="SigPnd:" {print $2}' /proc/$$/status)"
            if [ "$psign" -gt 0 ]; then
                totalsign=$(( totalsign + psign ))
                if [[ "$totalsign" -ge "$limitsi" ]]; then
                    echo "process of user $loopuser has $psign pending signals, while the soft limit is $limitsi (the hard limit is $limithi):"
                    echo "$i"
                fi
            fi
        done <<< "$procs"
        echo "User $loopuser has $totalf open files of $limitsn to $limithn, $totalprocs processes of $limitsu to $limithu and $totalsign pending signals of $limitsi to $limithi"
        echo "which are $(( 100* totalf / limithn))%, $(( 100* totalprocs / limithu))% and $(( 100* totalsign / limithi))% respectively"
        echo -e '\n'
    fi
done <<< "$limits_users"
echo -e '\n\n'


echo 'Filesystem        Size  Used Avail Use% Mounted on'
df -h | awk 'NR>1' | sort -r -k 5
echo -e '\n\n'
echo 'Filesystem           Inodes IUsed IFree IUse% Mounted on'
df -ih | awk 'NR>1' | sort -r -nk 5
echo -e '\n\n'


echo '==========     route     =========='
route
echo -e '\n\n'
echo '==========     ifconfig || ip addr show     =========='
ifconfig || ip addr show
echo -e '\n\n'

echo '==========     lsof -i     =========='
$MOD lsof -i
echo -e '\n\n\n'
echo '==========     netstat -tulnap     =========='
$MOD netstat -tulnap
echo -e '\n\n'

echo '==========     Processes sorted by CPU     =========='
ps -eo uname,pid,ppid,pcpu,pmem,args --sort=-pcpu |head
echo -e '\n\n\n'
echo '==========     Processes sorted by MEMORY     =========='
ps -eo uname,pid,ppid,pcpu,pmem,args --sort=-rss |head
echo -e '\n\n'

if [ -s /sys/class/thermal/thermal_zone0/temp ]; then #raspberry temp
    echo '==========     Temperature     =========='
    vcgencmd measure_temp && cat /sys/class/thermal/thermal_zone0/temp
    echo -e '\n\n'
fi


$MOD awk '$1~!/#/ && $2!="none" && $2!="/" {print $2}' /etc/fstab |while read -r i; do if [ -z "$(mount |awk '$3=="'$i'"')" ]; then  echo -e "==========     $i IS NOT MOUNTED     ==========\n\n" ; fi; done


echo '==========     uptime     =========='
uptime
echo -e '\n\n'

echo '==========     memory usage     =========='
free -h
#while read line; do
    #salida="$(echo $line |awk  ' {print $2} ')";
    #if [[ $line =~ "MemFree" ]]; then memfree="$salida"; elif [[ -n "$(echo $line | \grep "Active.file." )" ]]; then active="$salida"; elif [[ -n "$(echo $line | \grep "Inactive.file." )" ]]; then inactive="$salida"; elif [[ $line =~ "SReclaimable" ]]; then sreclaimable="$salida"; fi;
#done < /proc/meminfo; echo "Total de memoria libre es: $memfree + $active + $inactive + $sreclaimable : $((memfree + active + inactive + sreclaimable)) kB"
echo -e '\n\n'


echo '==========     amount of CPUs     =========='
grep '^processor' /proc/cpuinfo |wc -l
echo -e '\n\n'
