#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo 'No option specified'
elif [[ "$1" = 'start' ]]; then
    echo 'Starting tomcat8'
    sudo -u devops /opt/devops/tomcat/tomcat8/bin/startup.sh
elif [[ "$1" = 'stop' ]]; then
    echo 'Stopping tomcat8'
    sudo -u devops /opt/devops/tomcat/tomcat8/bin/shutdown.sh
elif [[ "$1" = 'restart' ]]; then
    echo 'Stopping and starting tomcat8'
    sudo -u devops /opt/devops/tomcat/tomcat8/bin/shutdown.sh && sleep 5 && sudo -u devops /opt/devops/tomcat/tomcat8/bin/startup.sh
fi

