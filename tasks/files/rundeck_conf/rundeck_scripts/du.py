#! /usr/bin/python3


import sys
import os
import argparse
import time


parser = argparse.ArgumentParser(description='recursive analysis of file size with conversion to several byte units and exclusions with filesystem info and listing the most consuming types and largests files')
parser.add_argument('directory', type=str, help='directory path')
parser.add_argument('-e', dest='exclude', nargs='+', type=str, help='strings to exclude in the search (default ""). Usage -e string1 string2 string3', required=False, default='')
parser.add_argument('-u', dest='unit', type=str, help='byte unit (default MiB): B, KB, MB, GB, KiB, MiB, GiB', choices=['B', 'KB', 'MB', 'GB', 'KiB', 'MiB', 'GiB'], required=False, default='MiB')
parser.add_argument('-m', dest='max_list_size', type=int, help='size of lists for largests types and files (default 10)', required=False, default=10)
parser.add_argument('-vm', dest='verbose_matches', type=bool, help='verbose matches (default False)', required=False, default=False, action=argparse.BooleanOptionalAction)
parser.add_argument('-ve', dest='verbose_errors', type=bool, help='verbose errors like non existing files (default False) or not enough permissions', required=False, default=False, action=argparse.BooleanOptionalAction)
parser.add_argument('-vp', dest='verbose_progress', type=bool, help='verbose progress every 2 minutes (default False)', required=False, default=False, action=argparse.BooleanOptionalAction)
args = parser.parse_args()

#eg: ./du.py ~/Downloads -u GiB -ve -vm -e excludeme -m 20


directory=args.directory
if not os.path.isdir(directory):
    print("directory",directory,"does not exist")
    exit(1)
excluding_strings=args.exclude
unit=args.unit
max_list_size=args.max_list_size
verbose_matches=args.verbose_matches
verbose_errors=args.verbose_errors
verbose_progress=args.verbose_progress



def calc_unit_size(size, u):
    if u=="B":
        return size
    if u=="KB":
        return round(size/1000,2)
    if u=="MB":
        return round(size/1000/1000,2)
    if u=="GB":
        return round(size/1000/1000/1000,2)
    if u=="KiB":
        return round(size/1024,2)
    if u=="MiB":
        return round(size/1024/1024,2)
    if u=="GiB":
        return round(size/1024/1024/1024,2)

def append_sorted(d, dkey, dval):
    if len(d)>=max_list_size:
        d_sorted=sorted(d.items(), key=lambda item: item[1])
        if d_sorted[0][1] < dval:
            d.pop(d_sorted[0][0])
            d[dkey]=dval
    else:
        d[dkey]=dval

def track_progress(verbose_progress, finished):
    if verbose_progress:
        global starttime
        global currenttime
        global prevtime
        global cfiles
        global ffiles
        currenttime=int(time.time())
        if finished or currenttime-prevtime > 120:
            prevtime=currenttime
            print("Total time elapsed:",currenttime-starttime,"seconds")
            print("Processed files:",cfiles)
            print("Failed or excluded files:",ffiles)
            print("")
            print("")

def is_contained(svalue,string_list):
    for i in string_list:
        if i in svalue:
            return True
    return False




prev_err=False
dict_types={}
dict_files={}
starttime=int(time.time())
currenttime=starttime
prevtime=starttime
cfiles=0
ffiles=0
total=0



#test if python can reach the directory with os.walk and change the current directory as a means to trigger the error message
len_oswalk=0
for root, dirs, files in os.walk(directory):
    len_oswalk+=len(root)
    len_oswalk+=len(dirs)
    len_oswalk+=len(files)
if len_oswalk==0:
    os.chdir(directory)



for root, dirs, files in os.walk(directory):
    for i in dirs:
        try:
            os.listdir(i)
        except:
            if verbose_errors:
                prev_err=True
                print("directory",root+os.sep+i,"cannot be accesed")

    for name in files:
        filepath=root+os.sep+name

        if is_contained(filepath,excluding_strings):
            ffiles+=1
            if verbose_matches:
                prev_err=True
                print(filepath,"matches one of the excluding strings")
            continue

        if not os.path.isfile(filepath):
            ffiles+=1
            if verbose_errors:
                prev_err=True
                print(filepath,"is not a valid file")
            continue

        try:
          sinfo = os.stat(filepath)
        except:
            ffiles+=1
            if verbose_errors:
                prev_err=True
                print(filepath,"could not be measured because of an error")
            continue

        total+=sinfo.st_size
        append_sorted(dict_files, filepath, sinfo.st_size)
        cfiles+=1
        track_progress(verbose_progress, False)

        if '.' in filepath:
            extension=str(filepath.rsplit('.',1)[-1])
            if extension in dict_types:
                dict_types[extension]+=sinfo.st_size
            else:
                dict_types[extension]=sinfo.st_size





if prev_err:
    print("")
    print("")

from subprocess import Popen, PIPE
p = Popen('df -h '+directory, shell=True, stdout=PIPE, stderr=PIPE)
out, err = p.communicate()
print("The state of the filesystem is")
print(out.decode("utf-8"))
#additional df of posible subfilesystems
dffilesystem=out.decode("utf-8").split()[-1]
if dffilesystem != '/':
    p = Popen("mount |grep "+dffilesystem+"| awk '{print $3}'", shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    mount_lines=out.decode("utf-8").splitlines()
    if len(mount_lines)>1:
        print("The state of other filesystems under", dffilesystem, "is")
        for i in range(1, len(mount_lines)):
            p = Popen('df -h '+mount_lines[i], shell=True, stdout=PIPE, stderr=PIPE)
            out, err = p.communicate()
            print(out.decode("utf-8"))


print("")
track_progress(verbose_progress, True)
print("The total size of", directory,"is")
print(calc_unit_size(total,unit),unit)

print("")
print("")
print("Most consuming types")
dict_types_sorted=sorted(dict_types.items(), key=lambda item: item[1])
for i in range(max(0,len(dict_types_sorted)-max_list_size),len(dict_types_sorted)):
    print(dict_types_sorted[i][0], calc_unit_size(dict_types_sorted[i][1],unit), unit)

print("")
print("")
print("Largest files")
dict_files_sorted=sorted(dict_files.items(), key=lambda item: item[1])
for i in range(max(0,len(dict_files_sorted)-max_list_size),len(dict_files_sorted)):
    print(dict_files_sorted[i][0], calc_unit_size(dict_files_sorted[i][1],unit), unit)
