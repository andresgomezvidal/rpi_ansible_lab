#!/usr/bin/env bash

#workaround because the free version is limited to localhost, so we need to connect and run scripts in other nodes ourselves

PORT=61640
REMOTE_HOST=192.168.0.
REMOTE_USER=devops

ARGS=""
SCRIPT_NAME=""
SCRIPT_LIBRARY="/opt/devops/rundeck/rundeck_scripts"
SCRIPT_LOCAL_PATH="$SCRIPT_LIBRARY/$SCRIPT_NAME"
SCRIPT_REMOTE_FOLDER=/tmp
SCRIPT_EXECUTION_PATH="$SCRIPT_REMOTE_FOLDER/$SCRIPT_NAME"

scp -P $PORT "$SCRIPT_LOCAL_PATH" $REMOTE_USER@$REMOTE_HOST:"$SCRIPT_EXECUTION_PATH"
ssh -p $PORT $REMOTE_USER@$REMOTE_HOST "$SCRIPT_EXECUTION_PATH" "$ARGS"
