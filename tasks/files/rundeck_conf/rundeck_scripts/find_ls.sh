#!/usr/bin/env bash

#list files and directories, including slinks

find -L "$@" |while read -r i; do if [[ -d "$i" && ! -L "$i" ]]; then ls -ld "$i"; else ls -alh "$i"; fi ; done

