#!/usr/bin/env bash

echo "
https://google.com
https://duckduckgo.com
https://tomcat.apache.org
" | while read -r i; do
    if [ -n "$i" ]; then
        salida="$(curl -m 75 -v --silent -kv "$i" 2>&1 )"
        #salida="$(curl -m 75 -v --silent -H 'Host: <web>' -kv "$i" 2>&1 )"
        if [ -n "$(grep 'Operation timed out after' <<< "$salida" )" ]; then
            echo "$i"
            echo "timed out"
        elif [ -z "$(grep 'HTTP/[1-2.]* 200' <<< "$salida" )" ]; then
            echo "$i"
            echo "$salida" |grep 'HTTP/[1-2.]* '
        fi
    fi
done
