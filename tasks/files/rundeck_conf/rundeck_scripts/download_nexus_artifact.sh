#!/usr/bin/env bash

#eg:
USERNAME='user'
PASSWORD='pass'
URL='http://192.168.0.109:8081/repository/genericproject/com/andresgomezvidal/genericproject/1.1-SNAPSHOT/genericproject-1.1-20230918.155245-4.war'
DEST='/tmp/artifact'

curl -v -u "$USERNAME":"$PASSWORD" -L -X GET "$URL" --output "$DEST"

