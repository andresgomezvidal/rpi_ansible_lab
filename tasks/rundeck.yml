
- hosts: rpi4b8-1
  become: yes
  gather_facts: yes
  remote_user: "{{ user_sudo }}"
  any_errors_fatal: true
  vars:
    user: rundeck
    postgresql_database: rundeck
    postgresql_user: rundeck
    systemd_services:
      - rundeck
  pre_tasks:
    - name: Install jdk 8 if needed
      include_tasks: include_tasks/jdk8.yml

    - name: Create the user rundeck
      user:
        name: rundeck
        group: "{{ user }}"
        groups: "{{ user_gui }},docker"
        home: "{{ rundeck_path }}/home"
        shell: /bin/bash
    - name: Create necessary directories
      file:
        path: "{{ item }}"
        state: directory
        owner: "{{ user }}"
        group: "{{ user_gui }}"
        mode: '0755'
      loop:
        - "{{ rundeck_path }}"
    - name: Allow rundeck to become {{ user_gui }} for tomcat management
      lineinfile:
        path: /etc/sudoers.d/rundeck
        regexp: '^rundeck ALL=({{ user_gui }}) NOPASSWD: {{ tomcat_path }}/tomcat8//bin/shutdown.sh'
        line: "rundeck ALL=({{ user_gui }}) NOPASSWD: {{ tomcat_path }}/tomcat8/bin/shutdown.sh"
        backup: yes
    - name: Allow rundeck to become {{ user_gui }} for tomcat management
      lineinfile:
        path: /etc/sudoers.d/rundeck
        regexp: '^rundeck ALL=({{ user_gui }}) NOPASSWD: {{ tomcat_path }}/tomcat8/bin/startup.sh'
        line: "rundeck ALL=({{ user_gui }}) NOPASSWD: {{ tomcat_path }}/tomcat8/bin/startup.sh"

    - name: Check if it is already installed
      stat:
        path: /etc/init.d/rundeckd
      register: rundeck_installed
    - name: Block for installing rundeck
      block:
      ## REQUIRES A LICENSE
      ## https://www.pagerduty.com/pricing/incident-response/
      ## https://docs.rundeck.com/docs/administration/install/installing-rundeck.html#installation
      #- name: Import the repo signing key
        #shell: curl -L https://packages.rundeck.com/pagerduty/rundeckpro/gpgkey | sudo apt-key add -
        #register: curl_key
      #- debug:
          #var=curl_key.stdout_lines
      #- name: Add package urls
        #copy:
          #dest: /etc/apt/sources.list.d/rundeck.list
          #owner: root
          #group: root
          #content: |
            #deb https://packages.rundeck.com/pagerduty/rundeckpro/any/ any main
            #deb-src https://packages.rundeck.com/pagerduty/rundeckpro/any/ any main
      #- name: Install rundeck
        #apt:
          #name: "{{ item }}"
          #update_cache: yes
        #with_items:
          #- rundeckpro-enterprise

      ## LIMITED TO LOCALHOST, WITHOUT OTHER WORKERS IN THE NETWORK
      ## you can kind of workaround it doing the ssh connection yourself in the script
      ## https://www.rundeck.com/downloads
      - name: Copy the deb
        copy:
          dest: "{{ rundeck_path }}"
          src: files/rundeck_4.16.0.20230825-1_all.deb
          owner: "{{ user }}"
          group: "{{ user_gui }}"
          mode: '0755'
      - name: Install rundeck
        apt:
          deb: "{{ rundeck_path }}/rundeck_4.16.0.20230825-1_all.deb"

      - name: Check if there is local config
        stat:
          path: files/rundeck_conf
        register: rundeck_conf_localhost
        become: false
        delegate_to: localhost
      - name: Copy initial config
        copy:
          dest: "{{ rundeck_path }}"
          src: files/rundeck_conf/
          owner: "{{ user }}"
          group: "{{ user_gui }}"
          mode: '0755'
        when: rundeck_conf_localhost.stat.exists
      - name: Create symbolic link
        file:
          src: /var/lib/rundeck
          dest: "{{ rundeck_path }}/lib"
          state: link
      - name: Create symbolic link
        file:
          src: /var/log/rundeck
          dest: "{{ rundeck_path }}/log"
          state: link
      when: not rundeck_installed.stat.exists


    #more config https://docs.rundeck.com/docs/administration/configuration/system-properties.html
    #- name: Get /etc/rundeck/rundeck-config.properties checksum
      #stat:
        #path: /etc/rundeck/rundeck-config.properties
        #checksum_algorithm: sha256
      #register: rundeck_properties
    - name: Block for configuring /etc/rundeck/rundeck-config.properties
      tags: etc_config
      block:
      - include_vars: roles/install_postgresql/vars/vault.yml
        when: postgresql_password is not defined or postgresql_password == None or postgresql_password == "" or postgresql_password == " "
      - name: set postgresql_password variable
        set_fact:
          postgresql_password: "{{ postgresql_default_password }}"
        when: postgresql_password is not defined or postgresql_password == None or postgresql_password == "" or postgresql_password == " "

      - name: Copy properties
        template:
          dest: /etc/rundeck/rundeck-config.properties
          src: templates/rundeck-config.properties.j2
          owner: root
          group: root
          mode: '0644'
      when: postgresql_user is defined
      #when: postgresql_user is defined and rundeck_properties.stat.checksum != "ed4a605c8b0fbaaf0ced2b6346a1ac7761aac2d1e646de54b968e18ec042fce2"



  post_tasks:
    - name: Waiting for port {{ rundeck_port }}
      wait_for:
        port: "{{ rundeck_port }}"
        timeout: 400
    - name: Check rundeck url is running
      uri:
        # http://192.168.0.109:4440
        url: http://{{ ansible_facts.default_ipv4.address }}:{{ rundeck_port }}
        status_code: 403

  roles:
    - install_postgresql
      ##- { role: install_postgresql, postgresql_database: 'rundeck', postgresql_user: 'rundeck' } #create a new database
    - { role: allow_firewall_port, firewall_ports: [ { number: "{{ rundeck_port }}", type: 'tcp'} ] }
    - add_enable_systemd_services

