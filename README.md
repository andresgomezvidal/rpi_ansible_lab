my home lab with some raspberries

my other raspberry repo https://gitlab.com/andresgomezvidal/rpi_ansible_home has been my tv for 4 years now

rpi4b8 steps start after manually setting up users, network, ssh, ssh keys and time zone
centos9 setps start from the very beginning (creating and setting up the vms)


localhost setps before starting:

    ansible-playbook tasks/ansible_dependencies.yml -K


centos9 only:

    ansible-playbook -i inventory_vagrant_initial tasks/add_centos_machines.yml


rpi4b8 only:

    ansible-playbook tasks/fs_node?.yml
    ansible-playbook tasks/firewalld.yml
    ansible-playbook tasks/change_hostname.yml
    ansible-playbook tasks/disable_wifi_bluetooth.yml # reboots


common playbooks for rpi4b8 and centos9:

    ansible-playbook tasks/upgrade.yml
    ansible-playbook tasks/programs.yml
    ansible-playbook tasks/fail2ban.yml
    ansible-playbook tasks/password_quality.yml
    ansible-playbook tasks/limit_su.yml
    ansible-playbook tasks/copy_ssh_keys.yml
    ansible-playbook tasks/nfs_client.yml
    ansible-playbook tasks/password_test.yml




infra:

    rpi4b4
        TODO dns server
    rpi4b8-1
        ansible-playbook tasks/fs_node1.yml
        ansible-playbook tasks/nexus.yml
            http://192.168.0.109:8081
        ansible-playbook tasks/tomcat.yml
            http://192.168.0.109:8080
            http://192.168.0.109:8080/genericproject/
        ansible-playbook tasks/docker.yml
        jenkins:
            #ansible-vault encrypt tasks/files/jenkins_conf/secret.key
            #ansible-vault encrypt tasks/files/jenkins_conf/secrets/*
            ansible-playbook tasks/jenkins.yml --ask-vault-pass
            http://192.168.0.109:8083
                plugins:
                 maven integration
                 pipeline utils (with pipeline)
                 nexus integration (without pipeline)
            https://gitlab.com/andresgomezvidal/rpi_devops_jenkins
                repo for java src, Jenkinsfile and Dockerfile
        ansible-playbook tasks/rundeck.yml --ask-vault-pass
            very restricted free version, but rundeck_workaround.sh has been working ok so far
            http://192.168.0.109:4440
    rpi4b8-2
        ansible-playbook tasks/fs_node2.yml
        ansible-playbook tasks/gitlab.yml
            http://192.168.0.110
                http://192.168.0.110:8080
        ansible-playbook tasks/docker.yml
        jenkins:
            ansible-playbook tasks/jenkins_node.yml
            node configured to run a jenkins agent
    centos9-node1
        ansible-playbook tasks/nfs_server.yml
    centos9-node2
    windows_local
        ansible-playbook tasks/windows_info.yml






